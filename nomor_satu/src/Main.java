import java.text.DecimalFormat;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        double num1 = 125.4;
        double num2 = 139.1;
        double num3 = 22.98;
        double num4 = 4.112;

        new DecimalFormat("#,###.##");
        new DecimalFormat("#,##.##");
        new DecimalFormat("#,##");

        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance(Locale.GERMAN);
        DecimalFormat df3 = (DecimalFormat) DecimalFormat.getInstance(Locale.GERMAN);
        DecimalFormat df4 = (DecimalFormat) DecimalFormat.getInstance(Locale.GERMAN);

        System.out.println(df.format(num1));
        System.out.println(df.format(num2));
        System.out.println(df3.format(num3));
        System.out.println(df4.format(num4));

    }
}
